BEGIN
  dbms_scheduler.create_job(job_name        => 'j_agregar_mes'
                           ,job_type        => 'STORED_PROCEDURE'
                           ,job_action      => 'dba_sosdw.p_nuevo_mes'
                           ,start_date      => '01/03/2020 09:20:00 -03:00'
                           ,repeat_interval => 'FREQ=MONTHLY;INTERVAL=1'
                           ,end_date        => NULL
                           ,auto_drop       => FALSE
                           ,enabled         => TRUE);
END;
